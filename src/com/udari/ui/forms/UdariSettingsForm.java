package com.udari.ui.forms;

import com.intellij.ide.DataManager;
import com.intellij.openapi.actionSystem.PlatformDataKeys;
import com.intellij.openapi.options.Configurable;
import com.intellij.openapi.options.ConfigurationException;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.wm.WindowManager;
import com.sun.istack.internal.NotNull;
import com.udari.UdariProjectComponent;
import com.udari.UdariSettings;
import com.udari.helpers.IdeHelper;
import org.jetbrains.annotations.Nls;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

/**
 * @author B G Kavinga on 2/21/16.
 */
public class UdariSettingsForm implements Configurable {
    private JPanel udariPanel;
    private JTextField pathToMagento;
    private JButton pathToMageButton;


    private Project project;

    public UdariSettingsForm(@NotNull final Project currentProject) {

        project = currentProject;
        pathToMageButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                browseButtonListener(e);
            }
        });

    }

    @Override
    public String getHelpTopic() {
        return null;
    }

    @Override
    public JComponent createComponent() {

        UdariSettings us = UdariSettings.getInstance(project);

        if (us instanceof UdariSettings) {
            pathToMagento.setText(us.getPathToMagento());
        }

        return udariPanel;

    }

    protected void browseButtonListener(ActionEvent e) {
        JFileChooser chooser = new JFileChooser();
        JTextField textField = pathToMagento;
        chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        chooser.setAcceptAllFileFilterUsed(false);
        String startPath = "";
        if (textField != null) {
            startPath = textField.getText();
        }
        if (startPath == null || startPath.isEmpty() || !(new File(startPath)).exists()) {
            UdariProjectComponent udari = UdariProjectComponent.getInstance(project);
            if (udari != null) {
                startPath = udari.getDefaultPathToMagento();
            }
        }
        if (startPath != null && !startPath.isEmpty()) {
            chooser.setCurrentDirectory(new File(startPath));
        }
        chooser.showOpenDialog(WindowManager.getInstance().suggestParentWindow(project));
        if (chooser.getSelectedFile() != null) {
            textField.setText(chooser.getSelectedFile().getAbsolutePath());
        }
    }

    @Override
    public void apply() throws ConfigurationException {
        try {

            DataManager dataManager = DataManager.getInstance();

            if (dataManager != null && PlatformDataKeys.PROJECT != null) {
                if (project != null) {
                    UdariSettings us = UdariSettings.getInstance(project);
                    if (us != null) {
                        us.setPathToMagento(pathToMagento.getText());
                    } else {
                        IdeHelper.logError("MaGinto MagintoSettings is null");
                    }

                } else {
                    IdeHelper.logError("Project is null");
                }
            } else {
                IdeHelper.logError("DataManager is null");
            }
        } catch (Exception e) {
            IdeHelper.logError("Unknown Error trying to save MaGinto settings");
            IdeHelper.logError(e.getMessage());
        }
    }

    @Override
    public boolean isModified() {
        UdariSettings udariSettings = UdariSettings.getInstance(project);

        return true;
    }

    @Override
    public void reset() {
        //To change body of implemented methods use File | MagintoSettings | File Templates.
    }

    @Override
    public void disposeUIResources() {
        //To change body of implemented methods use File | MagintoSettings | File Templates.
    }

    @Nls
    @Override
    public String getDisplayName() {
        return "MaGinto";
    }

}

package com.udari.ui.dialog;

import com.intellij.openapi.project.Project;
import com.intellij.openapi.ui.DialogWrapper;
import com.intellij.openapi.ui.ValidationInfo;
import com.udari.helpers.Magento;
import org.jetbrains.annotations.Nullable;

import javax.swing.*;
import java.util.ArrayList;

public class NewModelDialog extends DialogWrapper {
    private JPanel contentPanel;
    private JTextField className;
    private JTextField tableName;
    private JTextField primaryKey;
    private JComboBox moduleList;

    protected Project project;

    public NewModelDialog(Project project) {
        super(project);
        this.project = project;
        init();
        setTitle("New Model");
    }

    public void init() {
        super.init();
        ArrayList<String> modules = Magento.getAvailableModules(this.project);
        for (String module : modules) {
            moduleList.addItem(module);
        }
    }

    @Override
    protected JComponent createCenterPanel() {

        return contentPanel;
    }

    public String getClassName() {
        return className.getText();
    }

    public String getTableName() {

        return tableName.getText();
    }

    public String getPrimaryKey() {

        return primaryKey.getText();
    }

    public String getModuleName() {
        String selectedModule = (String) moduleList.getSelectedItem();
        String parts[] = selectedModule.split("_");
        return parts[1];

    }

    public String getCompanyName() {
        String selectedModule = (String) moduleList.getSelectedItem();
        String parts[] = selectedModule.split("_");
        return parts[0];

    }

    @Override
    public JComponent getPreferredFocusedComponent() {

        return className;
    }

    @Nullable
    protected ValidationInfo doValidate() {
        if (getClassName().equals("")) {
            return new ValidationInfo("Class name should not be empty", className);
        }
        if (getTableName().equals("")) {
            return new ValidationInfo("Table name should not be empty", tableName);
        }
        if (getPrimaryKey().equals("")) {
            return new ValidationInfo("Primary key should not be empty", tableName);
        }
        if (getModuleName().equals("")) {
            return new ValidationInfo("Module should not be empty", moduleList);
        }
        return null;
    }
}

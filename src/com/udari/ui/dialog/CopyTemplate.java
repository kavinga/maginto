package com.udari.ui.dialog;

import com.intellij.openapi.project.Project;
import com.intellij.openapi.ui.DialogWrapper;
import com.udari.helpers.Magento;
import com.udari.model.MagentoTheme;

import javax.swing.*;
import java.util.ArrayList;

public class CopyTemplate extends DialogWrapper {
    private JPanel contentPanel;
    private JComboBox themeList;

    protected Project project;

    public CopyTemplate(Project project) {
        super(project);
        this.project = project;
        init();
        setTitle("Copy Template");
    }

    public void init() {
        super.init();
        // populate magentoThemes
        ArrayList<MagentoTheme> magentoThemes = Magento.getAvailableThemes(project);
        for (MagentoTheme magentoTheme : magentoThemes) {
            themeList.addItem(magentoTheme);
        }

    }

    public MagentoTheme getSelectedTheme() {
        if (themeList.getSelectedItem() instanceof MagentoTheme) {
            return (MagentoTheme) themeList.getSelectedItem();
        }
        return null;
    }

    @Override
    protected JComponent createCenterPanel() {

        return contentPanel;
    }
}

package com.udari.ui.dialog;

import com.intellij.openapi.project.Project;
import com.intellij.openapi.ui.DialogWrapper;
import com.udari.helpers.Magento;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

/**
 * @author B G Kavinga on 4/13/16.
 */
public class NewGrid extends DialogWrapper {
    private JPanel contentPanel;
    private JComboBox moduleList;
    private JComboBox collectionList;
    private JComboBox layoutHandlerList;
    private JTextField reference;

    protected Project project;

    public NewGrid(Project project) {
        super(project);
        this.project = project;
        init();
        setTitle("Admin Grid");

    }

    public void init() {
        super.init();
        ArrayList<String> modules = Magento.getAvailableModules(this.project);
        for (String module : modules) {
            moduleList.addItem(module);
        }

        ArrayList<String> collections = Magento.getAvailableCollections(this.project);

        for (String collection : collections) {
            collectionList.addItem(collection);
        }
        updateHandlers();
        moduleList.addActionListener(new ModuleChangeListener(this));

    }

    public void updateHandlers() {
        ArrayList<String> handlers = Magento.getHandlersByModule(project, getCompanyName(), getModuleName());
        layoutHandlerList.removeAllItems();
        for (String handler : handlers) {
            layoutHandlerList.addItem(handler);
        }
    }

    @Override
    protected JComponent createCenterPanel() {

        return contentPanel;
    }

    public String getModuleName() {
        String selectedModule = (String) moduleList.getSelectedItem();
        String parts[] = selectedModule.split("_");
        return parts[1];

    }

    public String getCompanyName() {
        String selectedModule = (String) moduleList.getSelectedItem();
        String parts[] = selectedModule.split("_");
        return parts[0];

    }

    public String getSelectedHandler() {
        return layoutHandlerList.getSelectedItem().toString();
    }

    public String getReference() {
        return reference.getText();

    }


    protected class ModuleChangeListener implements ActionListener {

        protected NewGrid grid;

        public ModuleChangeListener(NewGrid grid) {
            this.grid = grid;
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            grid.updateHandlers();
        }
    }
}

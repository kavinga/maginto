package com.udari;

import com.intellij.openapi.components.*;
import com.intellij.openapi.project.Project;
import com.intellij.util.xmlb.XmlSerializerUtil;
import com.udari.helpers.IdeHelper;

import java.io.File;


/**
 * @author B G Kavinga on 2/21/16.
 */
@State(
        name = "UdariSettings",
        storages = {
                @Storage(id = "default", file = "$PROJECT_FILE$"),
                @Storage(id = "dir", file = "$PROJECT_CONFIG_DIR$/udari.xml", scheme = StorageScheme.DIRECTORY_BASED)
        }
)
public class UdariSettings implements PersistentStateComponent<UdariSettings> {

    protected String pathToMagento;

    protected Project project;

    protected String relativePathToMage = "/";

    @Override
    public UdariSettings getState() {
        return this;
    }

    @Override
    public void loadState(UdariSettings state) {
        try {
            XmlSerializerUtil.copyBean(state, this);
        } catch (Exception e) {
            IdeHelper.logError(e.getMessage());
            IdeHelper.showDialog(null, "Cannot read MaGinto settings", "Error in MaGinto MagintoSettings");
        }
    }


    @org.jetbrains.annotations.Nullable
    public static UdariSettings getInstance(Project project) {
        UdariSettings settings = ServiceManager.getService(project, UdariSettings.class);
        settings.project = project;
        return settings;
    }

    /**
     * Path to the root of Magento
     *
     * @return
     */
    public String getPathToMagento() {
        return pathToMagento;
    }

    public String getRelativePathToMage(Project project) {
        String path = getPathToMagento();
        String base = project.getBaseDir().getCanonicalPath();
        String relative = new File(base).toURI().relativize(new File(path).toURI()).getPath();
        return relative;
    }

    public void setPathToMagento(String pathToMagento) {
        this.pathToMagento = pathToMagento;
    }

}

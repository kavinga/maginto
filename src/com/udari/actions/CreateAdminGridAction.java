package com.udari.actions;

import com.intellij.openapi.actionSystem.AnActionEvent;
import com.intellij.openapi.command.WriteCommandAction;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.ui.DialogWrapper;
import com.intellij.openapi.vfs.VirtualFile;
import com.intellij.psi.PsiDirectory;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiFile;
import com.intellij.psi.PsiManager;
import com.intellij.psi.impl.source.xml.XmlFileImpl;
import com.intellij.psi.search.FilenameIndex;
import com.intellij.psi.search.GlobalSearchScope;
import com.intellij.psi.search.GlobalSearchScopes;
import com.intellij.psi.xml.XmlFile;
import com.intellij.psi.xml.XmlTag;
import com.udari.UdariIcons;
import com.udari.UdariSettings;
import com.udari.extensions.UdariTemplateFactory;
import com.udari.helpers.JavaHelper;
import com.udari.model.MagentoRoute;
import com.udari.ui.dialog.NewAdminGrid;
import org.jetbrains.annotations.Nullable;

import java.io.File;
import java.util.Properties;

/**
 * @author B G Kavinga on 4/15/16.
 */
public class CreateAdminGridAction extends AbstractAction {

    protected NewAdminGrid dialog = null;

    private String groupDirectoryPath = null;

    private String className = null;

    public CreateAdminGridAction() {
        super("Create Grid", "Create Admin Grid", UdariIcons.MAGENTO_ICON_16x16);
    }

    @Override
    protected PsiElement[] invokeDialogImpl(Project project, PsiDirectory directory) {
        dialog = new NewAdminGrid(project);
        dialog.show();
        if (dialog.getExitCode() == DialogWrapper.OK_EXIT_CODE) {
            String moduleName = dialog.getModuleName();
            String companyName = dialog.getCompanyName();
            MagentoRoute route = dialog.getSelectedRoute();
            String collection = dialog.getCollection();
            String group = dialog.getGroup();
            groupDirectoryPath = getDirectoryPathFromGroup(group);

            createController(companyName, moduleName);
//            createGridContainerPhp(companyName, moduleName, group);
//            createGridPhp(companyName, moduleName, group, collection);
            WriteCommandAction.runWriteCommandAction(project, new XmlUpdater(this, dialog));
        }
        dialog = null;
        return updatedElements;
    }

    @Nullable
    private PsiFile createRouteXml(MagentoRoute route, String companyName, String moduleName) {
        UdariSettings settings = UdariSettings.getInstance(project);
        String relativePath = settings.getRelativePathToMage(project) + "app/code/" + companyName + "/" + moduleName + "/etc/adminhtml/routes.xml";
        VirtualFile routeFile = project.getBaseDir().findFileByRelativePath(relativePath);
        // route xml file already exists
        if (routeFile != null) {
            PsiFile routePsiFile = PsiManager.getInstance(project).findFile(routeFile);
            if (routePsiFile instanceof XmlFileImpl) {
                XmlFile routeXml = (XmlFile) routePsiFile;
                XmlTag routerTag = null;
                for (XmlTag tag : routeXml.getRootTag().getSubTags()) {
                    if (tag.getName().equals("router") && tag.getAttributeValue("id").equals("admin")) {
                        routerTag = tag;
                        XmlTag routeTag = tag.findFirstSubTag("route");
                        if (routeTag != null) {
                            if (routeTag.getAttributeValue("frontName").equals(route.getRouteName())) {
                                return routePsiFile;
                            }
                        }
                    }
                }

                boolean newRouter = false;
                if (routerTag == null) {
                    routerTag = routeXml.getRootTag().createChildTag("router", null, null, false);
                    routerTag.setAttribute("id", "admin");
                    newRouter = true;
                }

                XmlTag routeTag = routerTag.createChildTag("route", null, null, false);
                routeTag.setAttribute("id", route.getRouteId());
                routeTag.setAttribute("frontName", route.getRouteName());
                XmlTag moduleTag = routeTag.createChildTag("module", null, null, false);
                moduleTag.setAttribute("name", companyName + "_" + moduleName);
                routeTag.addSubTag(moduleTag, false);
                routerTag.addSubTag(routeTag, false);
                if (newRouter) {
                    routeXml.getRootTag().addSubTag(routerTag, false);
                }
                return routePsiFile;
            }
        } else {
            createRouteXml(companyName, moduleName, "Adminhtml", route.getRouteName());
        }
        return null;
    }

    private PsiFile createRouteXml(String companyName, String moduleName, String area, String routeId) {
        final Properties properties = new Properties();
        properties.setProperty("MODULE", companyName + "_" + moduleName);
        properties.setProperty("ROUTEID", routeId);

        final String fileName = "routes.xml";
        String type = "standard";

        String directoryPath = pathToMagento + "/app/code/" + companyName + "/" + moduleName + "/etc/frontend";
        if (area.equals("Adminhtml")) {
            directoryPath = pathToMagento + "/app/code/" + companyName + "/" + moduleName + "/etc/adminhtml";
            type = "admin";
        }

        properties.setProperty("TYPE", type);

        PsiFile psiFile = UdariTemplateFactory.createFromTemplate(directoryPath,
                properties,
                fileName,
                UdariTemplateFactory.Template.RoutesXml,
                project);


        reformatFile(psiFile);

        return psiFile;
    }

    private PsiFile createGridContainerPhp(String companyName, String moduleName, String group) {
        final Properties properties = new Properties();


        properties.setProperty("BLOCKGROUP", companyName + "_" + moduleName);
        properties.setProperty("CONTROLLER", "adminhtml_" + group);
        properties.setProperty("HEADER", "Header " + group);
        properties.setProperty("NAMESPACE", companyName + "\\" + moduleName + "\\" + "Block\\Adminhtml");

        group = group.substring(0, 1).toUpperCase() + group.substring(1);
        properties.setProperty("CLASSNAME", group);
        final String fileName = group + ".php";
        String directoryPath = pathToMagento + "/app/code/" + companyName + "/" + moduleName + "/Block/Adminhtml";


        PsiFile psiFile = UdariTemplateFactory.createFromTemplate(directoryPath,
                properties,
                fileName,
                UdariTemplateFactory.Template.GridContainerPHP,
                project);


        reformatFile(psiFile);

        return psiFile;
    }

    private PsiFile createController(String companyName, String moduleName) {
        UdariSettings settings = UdariSettings.getInstance(project);
        String relativePath = settings.getRelativePathToMage(project) + "app/code/" + companyName + File.separator + moduleName + "/Controller/Adminhtml" + File.separator + groupDirectoryPath;
        String directoryPath = pathToMagento + JavaHelper.getParentDirPath(relativePath);
        String nameSpace = JavaHelper.getParentDirPath(companyName + File.separator + moduleName + "/Controller/Adminhtml" + File.separator + groupDirectoryPath);
        nameSpace = nameSpace.replaceAll(File.separator, "\\\\");
        Properties properties = new Properties();

        properties.setProperty("MODULENAME", companyName + "_" + moduleName);
        properties.setProperty("GROUP", groupDirectoryPath.replaceAll(File.separator, ""));
        properties.setProperty("NAMESPACE", nameSpace);
        properties.setProperty("CLASSNAME", className);

        String fileName = className + ".php";


        PsiFile psiFile = UdariTemplateFactory.createFromTemplate(directoryPath,
                properties,
                fileName,
                UdariTemplateFactory.Template.GridBaseActionPHP,
                project);


        reformatFile(psiFile);

        return psiFile;

    }

    private String getDirectoryPathFromGroup(String group) {
        String parts[] = group.split("_");
        String dir = "";
        for (String part : parts) {
            if (className == null) {
                className = part.substring(0, 1).toUpperCase() + part.substring(1);
            }
            dir += part.substring(0, 1).toUpperCase() + part.substring(1) + File.separator;
        }
        return dir.equals("") ? group : dir;
    }

    protected PsiFile createGridPhp(String companyName, String moduleName, String group, String collection) {
        final Properties properties = new Properties();

        properties.setProperty("GROUP", group);
        group = group.substring(0, 1).toUpperCase() + group.substring(1);
        properties.setProperty("HEADER", "Header " + group);
        properties.setProperty("NAMESPACE", companyName + "\\" + moduleName + "\\" + "Block\\Adminhtml\\" + group);
        properties.setProperty("CLASSNAME", "Grid");

        String collectionFactory = collection + "Factory";
        properties.setProperty("COLLECTIONFACTORY", collectionFactory);
        final String fileName = "Grid.php";
        String directoryPath = pathToMagento + "/app/code/" + companyName + "/" + moduleName + "/Block/Adminhtml/" + group;


        PsiFile psiFile = UdariTemplateFactory.createFromTemplate(directoryPath,
                properties,
                fileName,
                UdariTemplateFactory.Template.GridPHP,
                project);


        reformatFile(psiFile);

        return psiFile;
    }

    protected void updateLayoutXml(String companyName, String moduleName, String layoutXml, String group) {
        group = group.substring(0, 1).toUpperCase() + group.substring(1);
        UdariSettings settings = UdariSettings.getInstance(project);
        String relativePath = settings.getRelativePathToMage(project) + "app/code/" + companyName + "/" + moduleName + "/view/adminhtml/layout/";
        VirtualFile searchScope = project.getBaseDir().findFileByRelativePath(relativePath);
        String blockClass = companyName + "\\" + moduleName + "\\Block" + "\\" + "Adminhtml" + "\\" + group;
        GlobalSearchScope scope = null;
        PsiFile psiFiles[] = null;
        if (searchScope != null) {
            scope = GlobalSearchScopes.directoryScope(project, searchScope, true);
            psiFiles = FilenameIndex.getFilesByName(project, layoutXml, scope);
            for (PsiFile psiFile : psiFiles) {
                if (psiFile instanceof XmlFileImpl) {
                    XmlFileImpl xmlFile = (XmlFileImpl) psiFile;
                    String blockName = companyName + "_" + moduleName + "_" + group + "_grid_container";
                    blockName = blockName.toLowerCase();
                    XmlTag rootTag = xmlFile.getRootTag();
                    XmlTag containerTag = null;
                    XmlTag bodyTag = null;
                    XmlTag gridBlockTag = null;
                    for (XmlTag tag : rootTag.getSubTags()) {
                        if (tag.getName().equals("body")) {
                            bodyTag = tag;
                            for (XmlTag contentTag : tag.getSubTags()) {
                                if (contentTag.getName().equals("referenceContainer")) {
                                    if (contentTag.getAttribute("name").getValue().equals("content")) {
                                        containerTag = contentTag;
                                        for (XmlTag blockTag : containerTag.getSubTags()) {
                                            if (blockTag.getName().equals("block")) {
                                                if (blockTag.getAttribute("class").getValue().equals(blockClass)) {
                                                    gridBlockTag = blockTag;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }

                    if (!(bodyTag instanceof XmlTag)) {
                        bodyTag = rootTag.createChildTag("body", null, null, false);
                        containerTag = bodyTag.createChildTag("referenceContainer", null, null, false);
                        containerTag.setAttribute("name", "content");
                        gridBlockTag = containerTag.createChildTag("block", null, null, false);
                        gridBlockTag.setAttribute("class", blockClass);
                        gridBlockTag.setAttribute("name", blockName);
                        containerTag.addSubTag(gridBlockTag, false);
                        bodyTag.addSubTag(containerTag, false);
                        rootTag.addSubTag(bodyTag, false);
                    } else if (!(containerTag instanceof XmlTag)) {
                        containerTag = bodyTag.createChildTag("referenceContainer", null, null, false);
                        containerTag.setAttribute("name", "content");
                        gridBlockTag = containerTag.createChildTag("block", null, null, false);
                        gridBlockTag.setAttribute("class", blockClass);
                        gridBlockTag.setAttribute("name", blockName);
                        containerTag.addSubTag(gridBlockTag, false);
                        bodyTag.addSubTag(containerTag, false);
                    } else if (!(gridBlockTag instanceof XmlTag)) {
                        gridBlockTag = bodyTag.createChildTag("block", null, null, false);
                        gridBlockTag.setAttribute("class", blockClass);
                        gridBlockTag.setAttribute("name", blockName);
                        containerTag.addSubTag(gridBlockTag, false);
                    }
                }
            }
        }
    }

    private class XmlUpdater implements Runnable {
        private CreateAdminGridAction gridAction;
        private NewAdminGrid dialog;

        private XmlUpdater(CreateAdminGridAction gridAction, NewAdminGrid grid) {
            this.gridAction = gridAction;
            this.dialog = grid;
        }

        @Override
        public void run() {
            String moduleName = dialog.getModuleName();
            String companyName = dialog.getCompanyName();
            MagentoRoute magentoRoute = dialog.getSelectedRoute();
            gridAction.createRouteXml(magentoRoute, companyName, moduleName);
        }
    }

    public Boolean isApplicable(AnActionEvent e) {
        return true;
    }
}

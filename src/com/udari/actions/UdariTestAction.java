package com.udari.actions;

import com.intellij.ide.IdeView;
import com.intellij.openapi.actionSystem.*;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.ui.DialogWrapper;
import com.intellij.openapi.vfs.VirtualFile;
import com.intellij.psi.PsiDirectory;
import com.intellij.psi.PsiElement;
import com.udari.ui.dialog.TestActionDialog;
import org.jetbrains.annotations.NotNull;

/**
 * @author B G Kavinga on 2/28/16.
 */
public class UdariTestAction extends AbstractAction {

    private TestActionDialog myDialog;

    public UdariTestAction() {
        super("TestAction", "Description", null);
    }

    public void update(final AnActionEvent e) {
        super.update(e);

        final Presentation presentation = e.getPresentation();
        if (presentation.isEnabled()) {
            final DataContext context = e.getDataContext();

            final IdeView view = LangDataKeys.IDE_VIEW.getData(e.getDataContext());
            final Project project = CommonDataKeys.PROJECT.getData(e.getDataContext());
            final VirtualFile virtualFile = PlatformDataKeys.VIRTUAL_FILE.getData(context);
            if (view != null && project != null) {
                if (virtualFile instanceof VirtualFile && !virtualFile.isDirectory() && virtualFile.getExtension().equals("phtml")) {
                    return;
                }
            }

            presentation.setEnabled(false);
            presentation.setVisible(false);
        }
    }

    @NotNull
    @Override
    protected PsiElement[] invokeDialogImpl(Project project, PsiDirectory psiDirectory) {
        myDialog = new TestActionDialog(project);
        myDialog.show();
        if (myDialog.getExitCode() == DialogWrapper.OK_EXIT_CODE) {

        }
        myDialog = null;
        return PsiElement.EMPTY_ARRAY;
    }


}

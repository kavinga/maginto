package com.udari.actions;

import com.intellij.openapi.project.Project;
import com.intellij.openapi.ui.DialogWrapper;
import com.intellij.psi.PsiDirectory;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiFile;
import com.udari.UdariIcons;
import com.udari.extensions.UdariTemplateFactory;
import com.udari.ui.dialog.NewModelDialog;

import java.util.Properties;

/**
 * @author B G Kavinga on 2/22/16.
 */
public class CreateModelAction extends AbstractAction {

    protected NewModelDialog dialog;

    public CreateModelAction() {
        super("New Model", "Create New Magento 2 Model", UdariIcons.MAGENTO_ICON_16x16);
    }

    @Override
    protected PsiElement[] invokeDialogImpl(Project project, PsiDirectory directory) {
        dialog = new NewModelDialog(project);
        dialog.show();
        if (dialog.getExitCode() == DialogWrapper.OK_EXIT_CODE) {
            String className = dialog.getClassName();
            String tableName = dialog.getTableName();
            String primaryKey = dialog.getPrimaryKey();
            String moduleName = dialog.getModuleName();
            String companyName = dialog.getCompanyName();

            addUpdatedElement(createResourceModelPhpFile(moduleName, companyName, className, tableName, primaryKey));
            addUpdatedElement(createModelPhpFile(moduleName, companyName, className));
            addUpdatedElement(createResourceCollectionPhpFile(moduleName, companyName, className));

        }
        dialog = null;
        return updatedElements;
    }


    protected PsiFile createResourceCollectionPhpFile(String moduleName, String companyName, String className) {

        final Properties properties = new Properties();

        final String fileName = "Collection.php";
        String model = companyName + "\\" + moduleName + "\\Model\\" + className;
        String resourceModel = companyName + "\\" + moduleName + "\\Model\\ResourceModel\\" + className;

        properties.setProperty("RESOURCEMODEL", resourceModel);
        properties.setProperty("MODEL", model);
        properties.setProperty("NAMESPACE", companyName + "\\" + moduleName + "\\" + "Model\\ResourceModel\\" + className);

        String directoryPath = pathToMagento + "/app/code/" + companyName + "/" + moduleName + "/Model/ResourceModel/" + className;
        PsiFile psiFile = UdariTemplateFactory.createFromTemplate(directoryPath,
                properties,
                fileName,
                UdariTemplateFactory.Template.ResourceCollectionPHP,
                project);


        reformatFile(psiFile);

        return psiFile;

    }


    protected PsiFile createResourceModelPhpFile(String moduleName, String companyName, String className, String tableName, String primaryKey) {

        final Properties properties = new Properties();
        properties.setProperty("MODULENAME", moduleName);
        properties.setProperty("COMPANYNAME", companyName);

        final String fileName = className + ".php";
        String resourceModel = companyName + "\\" + moduleName + "\\Model\\ResourceModel\\" + className;

        properties.setProperty("RESOURCEMODEL", resourceModel);
        properties.setProperty("CLASSNAME", className);
        properties.setProperty("TABLE", tableName);
        properties.setProperty("PRIMARYKEY", primaryKey);
        properties.setProperty("NAMESPACE", companyName + "\\" + moduleName + "\\" + "Model\\ResourceModel");

        String directoryPath = pathToMagento + "/app/code/" + companyName + "/" + moduleName + "/Model/ResourceModel";
        PsiFile psiFile = UdariTemplateFactory.createFromTemplate(directoryPath,
                properties,
                fileName,
                UdariTemplateFactory.Template.ResourceModelPHP,
                project);


        reformatFile(psiFile);

        return psiFile;

    }

    protected PsiFile createModelPhpFile(String moduleName, String companyName, String className) {

        final Properties properties = new Properties();
        properties.setProperty("MODULENAME", moduleName);
        properties.setProperty("COMPANYNAME", companyName);

        final String fileName = className + ".php";
        String resourceModel = companyName + "\\" + moduleName + "\\Model\\ResourceModel\\" + className;

        properties.setProperty("RESOURCEMODEL", resourceModel);
        properties.setProperty("CLASSNAME", className);
        properties.setProperty("NAMESPACE", companyName + "\\" + moduleName + "\\" + "Model");

        String directoryPath = pathToMagento + "/app/code/" + companyName + "/" + moduleName + "/Model";
        PsiFile psiFile = UdariTemplateFactory.createFromTemplate(directoryPath,
                properties,
                fileName,
                UdariTemplateFactory.Template.ModelPHP,
                project);


        reformatFile(psiFile);

        return psiFile;

    }



}

package com.udari.actions;

import com.intellij.openapi.actionSystem.DefaultActionGroup;
import com.intellij.openapi.actionSystem.Presentation;
import com.intellij.openapi.project.DumbAware;
import com.udari.UdariIcons;

/**
 * @author B G Kavinga on 4/25/16.
 */
public class NewGroupAction extends DefaultActionGroup implements DumbAware {

    public NewGroupAction() {
        setPopup(true);
        Presentation presentation = getTemplatePresentation();
        presentation.setText("MaGinto");
        presentation.setIcon(UdariIcons.MAGENTO_ICON_16x16);

//        add(new UdariTestAction());
        add(new CreateModuleAction());
        add(new CreateModelAction());
        add(new CreateControllerAction());
        add(new CopyTemplateAction());
        add(new CopyLessAction());
        add(new CreateThemeAction());
//        add(new CreateAdminGridAction());

    }
}

package com.udari.actions;

import com.intellij.openapi.actionSystem.AnActionEvent;
import com.intellij.openapi.actionSystem.Presentation;
import com.intellij.openapi.command.WriteCommandAction;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.ui.DialogWrapper;
import com.intellij.openapi.vfs.VfsUtil;
import com.intellij.openapi.vfs.VirtualFile;
import com.intellij.psi.PsiDirectory;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiFile;
import com.intellij.psi.impl.source.xml.XmlFileImpl;
import com.intellij.psi.search.FilenameIndex;
import com.intellij.psi.search.GlobalSearchScope;
import com.intellij.psi.search.GlobalSearchScopes;
import com.intellij.psi.xml.XmlDocument;
import com.intellij.psi.xml.XmlTag;
import com.udari.UdariIcons;
import com.udari.helpers.IdeHelper;
import com.udari.model.MagentoTheme;
import com.udari.ui.dialog.CopyTemplate;

import java.io.File;
import java.io.IOException;

/**
 * @author B G Kavinga on 4/23/16.
 */
public class CopyTemplateAction extends AbstractAction {

    protected CopyTemplate dialog;


    public CopyTemplateAction() {
        super("Copy Template", "Copy template to theme", UdariIcons.MAGENTO_ICON_16x16);

    }

    @Override
    protected PsiElement[] invokeDialogImpl(Project project, PsiDirectory directory) {
        dialog = new CopyTemplate(project);
        dialog.show();
        if (dialog.getExitCode() == DialogWrapper.OK_EXIT_CODE) {
            MagentoTheme magentoTheme = dialog.getSelectedTheme();
            copyTemplate(magentoTheme);
        }
        dialog = null;
        return updatedElements;
    }


    private void copyTemplate(MagentoTheme magentoTheme) {
        VirtualFile target = selectedFile;
        if (target != null) {
            String paths[] = target.getParent().getPath().split("view/frontend/templates");
            if (paths.length > 0) {
                String targetModulePath = paths[0];
                String destinationFile = "";
                // template file under sub directory inside templates
                if (paths.length > 1) {
                    destinationFile = paths[1];
                }

                paths = targetModulePath.split(project.getBasePath());
                if (paths.length > 1 && paths[1] != null) {
                    targetModulePath = paths[1];
                }

                VirtualFile searchScope = project.getBaseDir().findFileByRelativePath(targetModulePath);
                GlobalSearchScope scope ;
                PsiFile psiFiles[] = {};
                String moduleName = null;
                if (searchScope != null) {
                    scope = GlobalSearchScopes.directoryScope(project, searchScope, true);
                    psiFiles = FilenameIndex.getFilesByName(project, "module.xml", scope);
                    if (psiFiles.length > 1) {
                        IdeHelper.logError("Unable to find target module ");
                        return;
                    }
                    for (PsiFile psiFile : psiFiles) {
                        XmlDocument doc = ((XmlFileImpl) psiFile).getDocument();
                        if (doc != null && doc.getRootTag() != null && doc.getRootTag().findFirstSubTag("module") != null) {
                            XmlTag tag = doc.getRootTag().findFirstSubTag("module");
                            if (tag != null) {
                                moduleName = tag.getAttributeValue("name");
                            }
                        }

                    }
                }
                if (moduleName != null) {
                    String parts[] = magentoTheme.getVirtualFile().getPath().split("theme.xml");
                    if (parts.length > 0) {
                        destinationFile = parts[0] + moduleName + File.separator + "templates" + destinationFile;
                        WriteCommandAction.runWriteCommandAction(project, new CopyTemplateRun(target, destinationFile));
                    }
                }
            } else {
                // check whether the selected file is in magentoTheme
                paths = target.getParent().getPath().split("templates");
                if (paths.length > 1) {
                    String path = paths[0];
                    String destinationFile = paths[1];
                    String moduleName = path.substring(path.lastIndexOf(File.separatorChar, path.length() - 2));
                    moduleName = moduleName.substring(1, moduleName.length() - 1);
                    if (!moduleName.equals("")) {
                        destinationFile = moduleName + File.separator + "templates" + destinationFile;
                        String parts[] = magentoTheme.getVirtualFile().getPath().split("theme.xml");
                        if (parts.length > 0) {
                            destinationFile = parts[0] + destinationFile;
                            WriteCommandAction.runWriteCommandAction(project, new CopyTemplateRun(target, destinationFile));

                        }

                    } else {
                        IdeHelper.logError("Invalid frontend template !");
                    }
                }
            }
        }
    }

    public void update(final AnActionEvent e) {
        super.update(e);
        final Presentation presentation = e.getPresentation();
        if (selectedFile != null && selectedFile.getExtension() != null) {
            if (selectedFile.getExtension().equals("phtml")) {
                return;
            }
        }
        presentation.setEnabled(false);
        presentation.setVisible(false);
    }


    private class CopyTemplateRun implements Runnable {

        private VirtualFile target;
        private String destinationPath;

        private CopyTemplateRun(VirtualFile target, String destinationPath) {
            this.target = target;
            this.destinationPath = destinationPath;
        }

        @Override
        public void run() {
            try {
                VirtualFile parent = VfsUtil.createDirectories(destinationPath);
                VirtualFile newFile = VfsUtil.copy(this, target, parent);
                openFile(newFile);
            } catch (IOException e) {
                e.printStackTrace();
                IdeHelper.logError("Unable to create parent directory " + destinationPath);
            }
        }
    }
}

package com.udari.actions;

import com.intellij.openapi.project.Project;
import com.intellij.openapi.ui.DialogWrapper;
import com.intellij.psi.PsiDirectory;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiFile;
import com.udari.UdariIcons;
import com.udari.extensions.UdariTemplateFactory;
import com.udari.ui.dialog.NewController;

import java.util.Properties;

/**
 * @author B G Kavinga on 4/12/16.
 */
public class CreateControllerAction extends AbstractAction {

    protected NewController dialog;


    public CreateControllerAction() {
        super("New Controller", "Create New Magento 2 Controller", UdariIcons.MAGENTO_ICON_16x16);

    }

    @Override
    protected PsiElement[] invokeDialogImpl(Project project, PsiDirectory directory) {
        dialog = new NewController(project);
        dialog.show();
        if (dialog.getExitCode() == DialogWrapper.OK_EXIT_CODE) {
            String area = dialog.getArea();
            String routeId = dialog.getRouteId();
            String moduleName = dialog.getModuleName();
            String companyName = dialog.getCompanyName();
            String group = dialog.getGroup().toLowerCase();
            if (group.equals("")) {
                group = "index";
            }
            addUpdatedElement(createControllerPhpFile(companyName, moduleName, area, group));
            addUpdatedElement(createRouteXml(companyName, moduleName, area, routeId));
            addUpdatedElement(createLayout(companyName, moduleName, area, routeId, group));
        }
        dialog = null;
        return updatedElements;
    }


    protected PsiFile createControllerPhpFile(String companyName, String moduleName, String area, String group) {
        final Properties properties = new Properties();
        group = group.substring(0, 1).toUpperCase() + group.substring(1);
        properties.setProperty("MODULENAME", moduleName);
        properties.setProperty("COMPANYNAME", companyName);
        properties.setProperty("CLASSNAME", "Index");
        properties.setProperty("NAMESPACE", companyName + "\\" + moduleName + "\\" + "Controller\\" + group);


        final String fileName = "Index.php";
        String directoryPath = pathToMagento + "/app/code/" + companyName + "/" + moduleName + "/Controller/" + group;

        String controllerClass = "Magento\\Framework\\App\\Action\\Action";

        if (area.equals("Adminhtml")) {
            directoryPath = pathToMagento + "/app/code/" + companyName + "/" + moduleName + "/Controller/Adminhtml/" + group;
            properties.setProperty("NAMESPACE", companyName + "\\" + moduleName + "\\" + "Controller\\Adminhtml\\" + group);
            controllerClass = "Magento\\Backend\\App\\Action";
        }

        properties.setProperty("CONTROLLER", controllerClass);

        PsiFile psiFile = UdariTemplateFactory.createFromTemplate(directoryPath,
                properties,
                fileName,
                UdariTemplateFactory.Template.ControllerPHP,
                project);


        reformatFile(psiFile);

        return psiFile;

    }

    protected PsiFile createRouteXml(String companyName, String moduleName, String area, String routeId) {
        final Properties properties = new Properties();
        properties.setProperty("MODULE", companyName + "_" + moduleName);
        properties.setProperty("ROUTEID", routeId);

        final String fileName = "routes.xml";
        String type = "standard";

        String directoryPath = pathToMagento + "/app/code/" + companyName + "/" + moduleName + "/etc/frontend";
        if (area.equals("Adminhtml")) {
            directoryPath = pathToMagento + "/app/code/" + companyName + "/" + moduleName + "/etc/adminhtml";
            type = "admin";
        }

        properties.setProperty("TYPE", type);

        PsiFile psiFile = UdariTemplateFactory.createFromTemplate(directoryPath,
                properties,
                fileName,
                UdariTemplateFactory.Template.RoutesXml,
                project);


        reformatFile(psiFile);

        return psiFile;
    }

    protected PsiFile createLayout(String companyName, String moduleName, String area, String routeId, String group) {
        final Properties properties = new Properties();

        final String fileName = routeId + "_" + group + "_index.xml";

        String directoryPath = pathToMagento + "/app/code/" + companyName + "/" + moduleName + "/view/frontend/layout";
        if (area.equals("Adminhtml")) {
            directoryPath = pathToMagento + "/app/code/" + companyName + "/" + moduleName + "/view/adminhtml/layout";
        } else {
            properties.setProperty("LAYOUT", "1");
        }


        PsiFile psiFile = UdariTemplateFactory.createFromTemplate(directoryPath,
                properties,
                fileName,
                UdariTemplateFactory.Template.LayoutXml,
                project);


        reformatFile(psiFile);

        return psiFile;
    }
}

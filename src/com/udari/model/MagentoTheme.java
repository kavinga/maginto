package com.udari.model;

import com.intellij.openapi.vfs.VirtualFile;

/**
 * @author B G Kavinga on 4/23/16.
 */
public class MagentoTheme implements Comparable<MagentoTheme>{
    protected String name;
    protected VirtualFile virtualFile;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public VirtualFile getVirtualFile() {
        return virtualFile;
    }

    public void setVirtualFile(VirtualFile virtualFile) {
        this.virtualFile = virtualFile;
    }

    public String toString() {
        return name;
    }

    @Override
    public int compareTo(MagentoTheme magentoTheme) {
        return this.getName().compareTo(magentoTheme.getName());
    }
}

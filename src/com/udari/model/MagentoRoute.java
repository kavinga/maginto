package com.udari.model;

/**
 * @author B G Kavinga on 5/8/16.
 */
public class MagentoRoute {


    private String routeId;

    private String routeName;

    private boolean isNew = false;

    public String getRouteId() {
        return routeId;
    }

    public void setRouteId(String routeId) {
        this.routeId = routeId;
    }

    public String getRouteName() {
        return routeName;
    }

    public void setRouteName(String routeName) {
        this.routeName = routeName;
    }

    public String toString() {
        return routeName;
    }

    public boolean isNew() {
        return isNew;
    }

    public void setNew(boolean aNew) {
        isNew = aNew;
    }
}

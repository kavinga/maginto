package com.udari.extensions;

import com.intellij.AbstractBundle;
import org.jetbrains.annotations.NonNls;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.PropertyKey;

/**
 * @author B G Kavinga on 4/24/16.
 */
public class UdariBundle extends AbstractBundle {

    @NonNls
    private static final String BUNDLE = "com.udari.extensions.UdariBundle";

    public static String message(@NotNull @PropertyKey(resourceBundle = BUNDLE) String key, @NotNull Object... params) {
        return ourInstance.getMessage(key, params);
    }


    private static final UdariBundle ourInstance = new UdariBundle();

    private UdariBundle() {
        super(BUNDLE);
    }
}

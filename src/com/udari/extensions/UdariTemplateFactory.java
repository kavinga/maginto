package com.udari.extensions;

import com.intellij.ide.fileTemplates.*;
import com.intellij.json.JsonLanguage;
import com.intellij.lang.Language;
import com.intellij.lang.StdLanguages;
import com.intellij.openapi.command.WriteCommandAction;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.vfs.LocalFileSystem;
import com.intellij.openapi.vfs.VfsUtil;
import com.intellij.openapi.vfs.VirtualFile;
import com.intellij.psi.PsiDirectory;
import com.intellij.psi.PsiFile;
import com.intellij.psi.PsiFileFactory;
import com.intellij.psi.PsiManager;
import com.jetbrains.php.lang.PhpLanguage;
import com.udari.UdariIcons;
import com.udari.helpers.IdeHelper;
import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.Nullable;

import java.io.File;
import java.io.IOException;
import java.util.Properties;

/**
 * @author B G Kavinga on 2/22/16.
 */
public class UdariTemplateFactory implements FileTemplateGroupDescriptorFactory {


    public enum Template {
        ModuleXml("udari_module.xml"),
        RegisterPHP("udari_registration.php"),
        ComposerJSON("udari_composer.json"),
        ModelPHP("udari_model.php"),
        ResourceModelPHP("udari_resource_model.php"),
        ResourceCollectionPHP("udari_collection_model.php"),
        RoutesXml("udari_routes.xml"),
        LayoutXml("udari_layout.xml"),
        DiXml("udari_di.xml"),
        InstallSchemaPHP("udari_install_schema.php"),
        InstallDataPHP("udari_install_data.php"),
        GridContainerPHP("udari_grid_container.php"),
        GridPHP("udari_grid.php"),
        GridBaseActionPHP("udari_grid_base_action.php"),
        ControllerPHP("udari_controller.php"),
        ViewXml("udari_view.xml"),
        ThemeXml("udari_theme.xml");

        String file;

        Template(String file) {
            this.file = file;
        }

        public String getFile() {
            return file;
        }
    }


    public FileTemplateGroupDescriptor getFileTemplatesDescriptor() {
        final FileTemplateGroupDescriptor group = new FileTemplateGroupDescriptor(UdariBundle.message("plugin.descriptor"), UdariIcons.MAGENTO_ICON_16x16);
        group.addTemplate(new FileTemplateDescriptor(Template.ModuleXml.getFile(), UdariIcons.MAGENTO_ICON_16x16));
        group.addTemplate(new FileTemplateDescriptor(Template.RegisterPHP.getFile(), UdariIcons.MAGENTO_ICON_16x16));
        group.addTemplate(new FileTemplateDescriptor(Template.ComposerJSON.getFile(), UdariIcons.MAGENTO_ICON_16x16));
        group.addTemplate(new FileTemplateDescriptor(Template.ModelPHP.getFile(), UdariIcons.MAGENTO_ICON_16x16));
        group.addTemplate(new FileTemplateDescriptor(Template.ResourceModelPHP.getFile(), UdariIcons.MAGENTO_ICON_16x16));
        group.addTemplate(new FileTemplateDescriptor(Template.ResourceCollectionPHP.getFile(), UdariIcons.MAGENTO_ICON_16x16));
        group.addTemplate(new FileTemplateDescriptor(Template.ControllerPHP.getFile(), UdariIcons.MAGENTO_ICON_16x16));
        group.addTemplate(new FileTemplateDescriptor(Template.RoutesXml.getFile(), UdariIcons.MAGENTO_ICON_16x16));
        group.addTemplate(new FileTemplateDescriptor(Template.LayoutXml.getFile(), UdariIcons.MAGENTO_ICON_16x16));
        group.addTemplate(new FileTemplateDescriptor(Template.DiXml.getFile(), UdariIcons.MAGENTO_ICON_16x16));
        group.addTemplate(new FileTemplateDescriptor(Template.InstallSchemaPHP.getFile(), UdariIcons.MAGENTO_ICON_16x16));
        group.addTemplate(new FileTemplateDescriptor(Template.InstallDataPHP.getFile(), UdariIcons.MAGENTO_ICON_16x16));
        group.addTemplate(new FileTemplateDescriptor(Template.GridContainerPHP.getFile(), UdariIcons.MAGENTO_ICON_16x16));
        group.addTemplate(new FileTemplateDescriptor(Template.GridPHP.getFile(), UdariIcons.MAGENTO_ICON_16x16));
        group.addTemplate(new FileTemplateDescriptor(Template.GridBaseActionPHP.getFile(), UdariIcons.MAGENTO_ICON_16x16));
        group.addTemplate(new FileTemplateDescriptor(Template.ThemeXml.getFile(), UdariIcons.MAGENTO_ICON_16x16));
        group.addTemplate(new FileTemplateDescriptor(Template.ViewXml.getFile(), UdariIcons.MAGENTO_ICON_16x16));
        return group;
    }


    private static PsiFile createFromTemplate(final PsiDirectory directory, Properties customProperties, String fileName, Template template, Project project) {

        String templateName = template.getFile();

        FileTemplate fileTemplate;
        Language type = null;

        fileTemplate = FileTemplateManager.getInstance(directory.getProject()).getInternalTemplate(templateName);

        type = getType(template);

        Properties properties = new Properties(FileTemplateManager.getInstance(directory.getProject()).getDefaultProperties());
        if (customProperties != null) {
            properties.putAll(customProperties);
        }

        String text;
        try {
            text = fileTemplate.getText(properties);
        } catch (Exception e) {
            throw new RuntimeException("Unable to load template for " + templateName, e);
        }

        final PsiFileFactory factory = PsiFileFactory.getInstance(directory.getProject());
        final PsiFile file = factory.createFileFromText(fileName, type, text);


        WriteCommandAction.runWriteCommandAction(project, new Runnable() {
            @Override
            public void run() {
                directory.add(file);
            }
        });

        return directory.findFile(fileName);

    }

    @Nullable
    @Contract(pure = true)
    private static Language getType(Template template) {
        switch (template) {
            case ModuleXml:
            case RoutesXml:
            case LayoutXml:
            case DiXml:
            case ThemeXml:
            case ViewXml:
                return StdLanguages.XML;
            case RegisterPHP:
            case ModelPHP:
            case ResourceModelPHP:
            case ResourceCollectionPHP:
            case ControllerPHP:
            case InstallDataPHP:
            case InstallSchemaPHP:
            case GridContainerPHP:
            case GridPHP:
            case GridBaseActionPHP:
                return PhpLanguage.INSTANCE;
            case ComposerJSON:
                return JsonLanguage.INSTANCE;
            default:
                return null;

        }
    }

    @Nullable
    public static PsiFile createFromTemplate(String directoryPath, Properties customProperties, String fileName, Template template, Project project) {
        VirtualFile directory = LocalFileSystem.getInstance().findFileByIoFile(new File(directoryPath));
        if (directory == null) {
            try {
                directory = VfsUtil.createDirectories(directoryPath);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        if (directory != null) {
            final PsiDirectory psiDirectory = PsiManager.getInstance(project).findDirectory(directory);
            if (psiDirectory != null) {
                if (psiDirectory.findFile(fileName) == null) {
                    return createFromTemplate(psiDirectory, customProperties, fileName, template, project);
                } else {
                    String message = "File " + directoryPath + File.separator + fileName + " already exists";
                    IdeHelper.logError(message);
                    IdeHelper.showDialog(project, message, "Cannot create new file");
                }
            }
        }
        return null;
    }
}

package com.udari.extensions;

import com.intellij.codeInsight.navigation.actions.GotoDeclarationHandler;
import com.intellij.openapi.actionSystem.DataContext;
import com.intellij.openapi.editor.Editor;
import com.intellij.openapi.project.Project;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiFile;
import com.intellij.psi.impl.source.xml.XmlTokenImpl;
import com.intellij.psi.search.FilenameIndex;
import com.intellij.psi.search.ProjectScope;
import com.intellij.psi.xml.XmlToken;
import com.intellij.util.ArrayUtil;
import org.jetbrains.annotations.Nullable;

import java.io.File;

/**
 * @author B G Kavinga on 5/1/16.
 */
public class UdariGotoDeclarationHandler implements GotoDeclarationHandler {


    @Nullable
    @Override
    public PsiElement[] getGotoDeclarationTargets(@Nullable PsiElement psiElement, int i, Editor editor) {
        if (!(psiElement instanceof XmlToken)) {
            return null;
        }
        Project project = editor.getProject();

        if (project == null) {
            project = psiElement.getProject();
        }
        XmlTokenImpl xmlToken = (XmlTokenImpl) psiElement;
        if (isValid(xmlToken)) {
            return getFiles(project, getFilePath(xmlToken));
        }
        return null;
    }

    @Nullable
    @Override
    public String getActionText(DataContext dataContext) {
        return "My Action Text";
    }

    private boolean isValid(XmlTokenImpl xmlToken) {
        boolean isValid = false;
        if (xmlToken.getText().endsWith(".phtml")) {
            PsiFile containingFile = xmlToken.getContainingFile();
            String path = containingFile.getVirtualFile().getPath();
            if (path.contains(File.separator + "layout" + File.separator)) {
                isValid = true;
            }
        }
        return isValid;
    }


    private String getFilePath(XmlTokenImpl xmlToken) {
        String value = xmlToken.getText();
        String path = value;
        // if path has module name
        String parts[] = value.split("::");
        if (parts.length > 1) {
            path = parts[1];
        }
        return path;
    }

    private PsiFile[] getFiles(Project project, String filePath) {
        PsiFile filteredFiles[] = {};
        int start = filePath.lastIndexOf('/');
        String fileName = filePath.substring(start == -1 ? 0 : start + 1);
        PsiFile[] psiFiles = FilenameIndex.getFilesByName(project, fileName, ProjectScope.getProjectScope(project));
        if (psiFiles.length > 0) {
            for (PsiFile psiFile : psiFiles) {
                String fullPath = psiFile.getVirtualFile().getPath();
                if (fullPath.endsWith(filePath)) {
                    filteredFiles = ArrayUtil.append(filteredFiles, psiFile);
                }
            }
        }
        return filteredFiles;
    }


}
